# Quick tour

Trying the huggingface <a href="https://github.com/huggingface/transformers#quick-tour">Quick tour</a>.

Made a naive implementation that loads the models for DistilBertModel.

This resulted in these models on disk:

```
$ ls -l models/
total 28
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:54 BertForMaskedLM
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:54 BertForNextSentencePrediction
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:53 BertForPreTraining
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:55 BertForQuestionAnswering
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:54 BertForSequenceClassification
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:55 BertForTokenClassification
drwxrwxr-x 2 peter_v peter_v 4096 May 17 11:53 BertModel

# Each model has these files:

$ ls -l models/BertModel/
total 427952
-rw-rw-r-- 1 peter_v peter_v       450 May 17 14:14 config.json
-rw-rw-r-- 1 peter_v peter_v 437971732 May 17 14:14 pytorch_model.bin
-rw-rw-r-- 1 peter_v peter_v       112 May 17 14:14 special_tokens_map.json
-rw-rw-r-- 1 peter_v peter_v        48 May 17 14:14 tokenizer_config.json
-rw-rw-r-- 1 peter_v peter_v    231508 May 17 14:14 vocab.txt
```
