# Trying TensorFlow 2 classes

As documented https://github.com/huggingface/transformers#quick-tour we should be able
to use TensorFlow 2.0 classes by renaming the class to e.g. `TFDistilBertModel`.

This kind of works but hits the problem that Tensorflow 2.2 does not support libcuda 10.2
and specifically for `libcudart` this causes the problem:

```
2020-05-17 15:31:30.128878: W tensorflow/stream_executor/platform/default/dso_loader.cc:55] Could not load dynamic library 'libcudart.so.10.1'; dlerror: libcudart.so.10.1: cannot open shared object file: No such file or directory
2020-05-17 15:31:30.130972: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10
2020-05-17 15:31:30.132830: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcufft.so.10
2020-05-17 15:31:30.133473: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcurand.so.10
2020-05-17 15:31:30.136254: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusolver.so.10
2020-05-17 15:31:30.138269: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusparse.so.10
2020-05-17 15:31:30.143510: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudnn.so.7
2020-05-17 15:31:30.143749: W tensorflow/core/common_runtime/gpu/gpu_device.cc:1598] Cannot dlopen some GPU libraries. Please make sure the missing libraries mentioned above are installed properly if you would like to use GPU. Follow the guide at https://www.tensorflow.org/install/gpu for how to download and setup the required libraries for your platform.
```

Turns out that Tensorflow 2.2 expects libcudart 10.1, but also that 10.2 and 10.1 are compatible,
so a symlink can be a working hack. This hack is documented here:

https://github.com/tensorflow/tensorflow/issues/38194#issuecomment-629801937

And yields a running test on the nvidia GPU in my Dell Precision 5520 laptop, see

https://twitter.com/peter_v/status/1262020347169320960

Later on the result from this test fail with:

```
Traceback (most recent call last):
  File "/home/peter_v/data/gitlab/spelfouten/pytorch-transformers/src/quick_tour.py", line 42, in <module>
    last_hidden_states = model(input_ids)[0]  # Models outputs are now tuples
  File "/home/peter_v/.local/lib/python3.8/site-packages/tensorflow/python/keras/engine/base_layer.py", line 968, in __call__
    outputs = self.call(cast_inputs, *args, **kwargs)
  File "/home/peter_v/.local/lib/python3.8/site-packages/transformers/modeling_tf_distilbert.py", line 573, in call
    outputs = self.distilbert(inputs, **kwargs)
  File "/home/peter_v/.local/lib/python3.8/site-packages/tensorflow/python/keras/engine/base_layer.py", line 968, in __call__
    outputs = self.call(cast_inputs, *args, **kwargs)
  File "/home/peter_v/.local/lib/python3.8/site-packages/transformers/modeling_tf_distilbert.py", line 437, in call
    input_shape = shape_list(input_ids)
  File "/home/peter_v/.local/lib/python3.8/site-packages/transformers/modeling_tf_utils.py", line 1694, in shape_list
    static = x.shape.as_list()
AttributeError: 'torch.Size' object has no attribute 'as_list'
```

I will switch back to PyTorch for this repository and execute tensorflow tests in another repo.
