# pytorch-transformers

PyTorch transformers experiments.

Ref. https://github.com/huggingface/transformers

## Installing cuda locally

I <a href="https://gitlab.com/spelfouten/core/-/blob/master/docs/raw/01_install_cuda.md">installed</a> <a href="https://developer.nvidia.com/cuda-zone">cuda</a> locally
and ran a [basic test](./src/cuda_test.py) that reports:

```
$ pipenv run python test_cuda.py
tensor([[0.8948, 0.9422, 0.7916],
        [0.5155, 0.8133, 0.0952],
        [0.6591, 0.9844, 0.5680],
        [0.0709, 0.0901, 0.0926],
        [0.1524, 0.7646, 0.8921]])
True
```
